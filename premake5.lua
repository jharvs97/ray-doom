workspace "ray-doom"
    configurations { "Debug", "Release" }
    architecture "x86_64"

project "ray-doom"
    kind "ConsoleApp"
    language "C++"
    cppdialect "c++20"
    targetdir "bin/%{cfg.buildcfg}"

    libdirs { "./external/raylib/lib/" }
    includedirs { "./external/raylib/include" }

    files { "./code/**.hpp", "./code/**.h", "./code/**.cpp" }

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

    filter "system:windows"
        links { "winmm", "raylib" }

    