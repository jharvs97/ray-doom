#include "arena_allocator.hpp"
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <cassert>

arena_allocator::arena_allocator()
{
	init(kilobytes(16));
}

arena_allocator::arena_allocator(size_t arena_size)
{
	init(arena_size);
}

void arena_allocator::init(size_t arena_size)
{
	this->bytes_written = 0;

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	
	this->page_size = info.dwPageSize;
	this->arena_size = arena_size;
	
	memory = (uint8_t*)VirtualAlloc(NULL, arena_size, MEM_RESERVE, PAGE_READWRITE);
	commit_head = memory;
}


uint8_t* arena_allocator::alloc(size_t amount)
{
	if ( (memory + (bytes_written + amount)) > commit_head )
	{
		if (!commit(amount))
		{
			return NULL;
		}
	}

	auto address = memory + bytes_written;
	bytes_written += amount;
	return address;
}

bool arena_allocator::commit(const size_t required_size)
{
	auto alloc_size = (required_size / page_size + 1) * page_size;
	assert(bytes_written + alloc_size <= arena_size);
	auto alloc_result = VirtualAlloc(commit_head, alloc_size, MEM_COMMIT, PAGE_READWRITE);
	
	if (alloc_result == NULL)
	{
		return false;
	}

	commit_head += alloc_size;

	return true;
}
