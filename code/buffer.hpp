#pragma once

#include <cstdint>
#include <bit>
#include <cassert>
#include "arena_allocator.hpp"

template<class T>
struct buffer
{
    T* data;
    size_t size;
    
    buffer() = default;

    buffer(arena_allocator& arena, const size_t size_in_bytes)
    {
        this->data = std::bit_cast<T*>(arena.alloc(size_in_bytes));
        this->size = size_in_bytes;
    }

    void copy(const buffer<uint8_t> source_bytes)
    {
        assert(source_bytes.size <= this->size);
        memcpy(this->data, source_bytes.data, source_bytes.size);
    }

    void copy(const uint8_t* source, const size_t size)
    {
        assert(size <= this->size);
        memcpy(this->data, source, size);
    }

    T& operator[](size_t index)
    {
        return data[index];
    }
};

struct byte_span
{
private:
    uint8_t* start;
    uint8_t* sentinel;
    uint8_t* cursor;

public:
    byte_span(uint8_t* start, uint8_t* sentinel)
    {
        this->start = start;
        this->cursor = start;
        this->sentinel = sentinel;
    }

    byte_span(buffer<uint8_t> buf)
    {
        this->start = buf.data;
        this->cursor = buf.data;
        this->sentinel = buf.data + buf.size;
    }

    uint8_t* get_cursor()
    {
        return cursor;
    }

    template <class T>
    constexpr T get()
    {
        // @Todo: Endianess?
        assert((cursor + sizeof(T)) < sentinel);
        auto result = std::bit_cast<T*>(cursor);
        auto next = (uint8_t*)((T*)cursor + 1);
        assert(next < sentinel);
        cursor = next;
        return *result;
    }

    void offset_cursor_from_start_by(int64_t offset)
    {
        assert((start + offset < sentinel) && (start + offset >= start));
        cursor = start + offset;
    }
};

