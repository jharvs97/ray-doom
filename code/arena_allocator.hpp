#pragma once

#include <cstdint>

consteval size_t kilobytes(int n)
{
	return 1024ll * n;
}

consteval size_t megabytes(int n)
{
	return kilobytes(1024) * n;
}

consteval size_t gigabytes(int n)
{
	return megabytes(1024) * n;
}

struct arena_allocator
{
	arena_allocator();
	arena_allocator(size_t arena_size);
	uint8_t* alloc(size_t amount);

private:
	uint8_t* memory;
	uint8_t* commit_head;
	size_t bytes_written;
	size_t arena_size;
	size_t page_size;

	void init(size_t arena_size);
	bool commit(const size_t required_size);
};