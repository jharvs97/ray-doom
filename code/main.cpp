#include "raylib.h"
#include "WAD.hpp"
#include "arena_allocator.hpp"

int main()
{
    arena_allocator wad_arena(gigabytes(1));

    WAD_file wad_file = WAD_file::load(wad_arena, "data/DOOM.WAD");
    WAD_map_file map = wad_file.load_map_by_name(wad_arena, "E1M1");

    const int screenWidth = 800;
    const int screenHeight = 450;
    
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
    
    SetTargetFPS(60);
    
    while (!WindowShouldClose())
    {
        BeginDrawing();
        
        ClearBackground(RAYWHITE);
        
        DrawText("Congrats! You created your first window!", 190, 200, 20, LIGHTGRAY);
        
        EndDrawing();
    }
    
    CloseWindow();

    return 0;
}