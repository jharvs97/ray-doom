#pragma once

#include <cstdint>
#include "buffer.hpp"
#include "arena_allocator.hpp"

struct WAD_string
{
    int8_t buf[8];

    bool operator==(const WAD_string& other)
    {
        return memcmp(this->buf, other.buf, 8) == 0;
    }

    bool operator==(const char* other)
    {
        return memcmp(this->buf, other, 8) == 0;
    }

    int8_t& operator[](size_t index)
    {
        return buf[index];
    }
};

struct WAD_header
{
    int8_t signature[4];
    int32_t num_files;
    int32_t info_table_offset;
};

struct WAD_file_entry
{
    int32_t file_offset;
    int32_t size_in_bytes;
    WAD_string name;
};

struct WAD_thing
{
    int16_t x;
    int16_t y;
    int16_t facing_angle;
    int16_t thing_type;
    int16_t flags;
};

struct WAD_linedef
{
    int16_t start_vertex;
    int16_t end_vertex;
    int16_t flags;
    int16_t special_type;
    int16_t sector_tag;
    int16_t front_sidedef;
    int16_t back_sizedef;
};

struct WAD_sidedef
{
    int16_t x_offset;
    int16_t y_offset;
    WAD_string upper_tex_name;
    WAD_string lower_tex_name;
    WAD_string middle_tex_name;
    int16_t facing_sector_num;
};

struct WAD_vertex
{
    int16_t x;
    int16_t y;
};

struct WAD_segment
{
    int16_t starting_vertex;
    int16_t ending_vertex;
    int16_t angle;
    int16_t linedef_num;
    int16_t direction; // 0 (same as linedef) or 1 (opposite of linedef)
    int16_t offset; // distance along linedef to start of seg
};

struct WAD_subsector
{
    int16_t segment_count;
    int16_t first_seg_num;
};

struct WAD_node
{
    int16_t	x;// coordinate of partition line start
    int16_t	y;// coordinate of partition line start
    int16_t	delta_x;// Change in x from start to end of partition line
    int16_t	delta_y;// Change in y from start to end of partition line
    int16_t right_bounds[4];// Right bounding box
    int16_t left_bounds[4];// Left bounding box
    int16_t	right_child;// Right child
    int16_t	left_child;// Left child
};

struct WAD_sector
{
    int16_t	floor_height;// Floor height
    int16_t	ceil_height;// Ceiling height
    WAD_string floor_tex_name;//	Name of floor texture
    WAD_string ceil_tex_name;// Name of ceiling texture
    int16_t	light_level;// Light level
    int16_t	special_type;// Special Type
    int16_t	tag_num;// Tag number
};

struct WAD_map_file
{
    buffer<WAD_thing> things;
    buffer<WAD_linedef> linedefs;
    buffer<WAD_sidedef> sidedefs;
    buffer<WAD_vertex> vertexes; // vertices?
    buffer<WAD_segment> segments;
    buffer<WAD_subsector> subsectors;
    buffer<WAD_node> nodes;
    buffer<WAD_sector> sectors;
};

struct WAD_file
{
    WAD_header header;
    WAD_file_entry* file_entries;
    buffer<uint8_t> all_bytes;

    WAD_map_file load_map_by_name(arena_allocator&, const char*);
    static WAD_file load(arena_allocator&, const char*);

    template<class T>
    buffer<T> get_map_data(arena_allocator& arena, const WAD_file_entry& entry, byte_span& file_span)
    {
        file_span.offset_cursor_from_start_by(entry.file_offset);
        buffer<T> buf(arena, entry.size_in_bytes);
        buf.copy(file_span.get_cursor(), entry.size_in_bytes);
        return buf;
    }
};

