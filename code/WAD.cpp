#include "wad.hpp"
#include "raylib.h"
#include <cassert>

// @Todo: Allocators!
WAD_file WAD_file::load(arena_allocator& arena, const char* file_name)
{
    WAD_file result{};

    uint32_t bytes_read = 0;
    uint8_t* file_data = LoadFileData(file_name, &bytes_read);

    result.all_bytes = buffer<uint8_t>(arena, bytes_read);
    result.all_bytes.copy(file_data, bytes_read);

    byte_span span{ result.all_bytes };

    result.header = span.get<WAD_header>();

    span.offset_cursor_from_start_by(result.header.info_table_offset);

    result.file_entries = (WAD_file_entry*) arena.alloc(sizeof(WAD_file_entry) * (result.header.num_files + 1));
    memcpy(result.file_entries, file_data + result.header.info_table_offset, sizeof(WAD_file_entry) * result.header.num_files);

    UnloadFileData(file_data);

    return result;
}

WAD_map_file WAD_file::load_map_by_name(arena_allocator& arena, const char* map_name)
{
    WAD_map_file result{};

    int32_t map_file_entry_index = -1;
    for (int32_t i = 0; i < header.num_files; i++)
    {
        auto entry = file_entries[i];
        if (entry.name == map_name)
        {
            map_file_entry_index = i;
        }
    }

    assert(map_file_entry_index != -1);

    byte_span file_span{ all_bytes };
    result.things = get_map_data<WAD_thing>(arena, file_entries[++map_file_entry_index], file_span);
    result.linedefs = get_map_data<WAD_linedef>(arena, file_entries[++map_file_entry_index], file_span);
    result.sidedefs = get_map_data<WAD_sidedef>(arena, file_entries[++map_file_entry_index], file_span);
    result.vertexes = get_map_data<WAD_vertex>(arena, file_entries[++map_file_entry_index], file_span);
    result.segments = get_map_data<WAD_segment>(arena, file_entries[++map_file_entry_index], file_span);
    result.subsectors = get_map_data<WAD_subsector>(arena, file_entries[++map_file_entry_index], file_span);
    result.nodes = get_map_data<WAD_node>(arena, file_entries[++map_file_entry_index], file_span);
    result.sectors = get_map_data<WAD_sector>(arena, file_entries[++map_file_entry_index], file_span);

    return result;
}